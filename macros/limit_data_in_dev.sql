{%- macro limit_data_in_dev(column_name, days_to_show) -%}
{% if target.name == 'dev' %}
where {{ column_name }} >= dateadd('day', -3, {{ days_to_show }})
{% endif %}
{%- endmacro -%}
