# HelloDbt

[dbt](https://www.getdbt.com/) demo project.
Based on the [dbt Fundamentals](https://courses.getdbt.com/courses/fundamentals) course.


## Overview

The project incorporates the following components:
- This repository for source control.
- [dbt-cloud](https://cloud.getdbt.com/) account.
- Local [dbt CLI](https://docs.getdbt.com/dbt-cli/cli-overview).

All connected to a BigQuery demo project and authenticated via a [service account JSON file](https://cloud.google.com/bigquery/docs/authentication/service-account-file).

## Models

Models are SQL files that live in the `models/` folder.
They are simply written as select statements - there is no DDL/DML that needs to be written around this.

After constructing a model, `dbt run` in the command line will actually materialize the models.
Alternatively, use `dbt run --models dim_customers` to compile a single model.

### Conventions
Some conventions for naming the models were established:
- **Sources** refer to the raw table data that have been built in the warehouse through a loading process. 
- **Staging** refers to models that are built directly on top of sources.
- **Intermediate** refers to any models that exist between final fact and dimension tables.
- **Fact** refers to any data that represents something that occurred or is occurring.
- **Dimension** refers to data that represents a person, place or thing.

## Tests

In Analytics Engineering, testing allows us to make sure that the SQL transformations we write produce a model that meets the assertions.
They are written as select statements.

Use `dbt test` to run the testing suite.

### Schema tests

Schema tests are written in YAML and return the number of records that do not meet your assertions.
These are run on specific columns in a model.

dbt ships with four built in tests:
- **Unique** tests to see if every value in a column is unique.
- **Not null** tests to see if every value in a column is not null.
- **Accepted values** tests to make sure every value in a column is equal to a value in a provided list.
- **Relationships** tests to ensure that every value in a column exists in a column in another model.

In addition to that, custom tests can be implemented using Jinja and macros.

### Data tests

Data tests are specific queries that you run against your models.
A data test passes if the number of records returned is 0.
These are run on the entire model from the `tests/` directory.

Execute `dbt test --data` to run only data tests in your project.

## Documentation

In dbt, models are documented in YML files that live in the same folder as the models.

If a longer form, more styled version of text would provide a strong description, doc blocks can be used to render markdown in the generated documentation.

An updated version of documentation can be generated through the command `dbt docs generate`.

The generated documentation includes the following:
- Lineage Graph
- Model and column descriptions
- Schema tests added to a column
- The underlying SQL code for each model

## Sources

Sources represent the raw data that is loaded into the data warehouse.

Setting up Sources in dbt and referring to them with the source function enables a few important tools:
- Multiple tables from a single source can be configured in one place.
- Sources are easily identified in the Lineage Graph when viewing documentation.
- Text and Doc Block can be used to add descriptions to sources.
- Tests can be run directly on sources before you run tests on models that depend on them.
- You can use `dbt source snapshot-freshness` to check the freshness of raw tables.

## Deployment

Deployment in dbt (or running dbt in production) is the process of running dbt on a schedule in a deployment environment.

This can be done in dbt cloud or using 3rd party schedulers.

Note: When deploying a real dbt Project, you should set up a separate data warehouse account for this run.
This should not be the same account that you personally use in development.

## Macros

Macros are functions that are written in Jinja.
This allows us to write generic logic once, and then reference that logic throughout our project.

Macros allow us to write DRY (Don’t Repeat Yourself) code in our dbt project.
This allows us to take one model file that was 200 lines of code and compress it down to 50 lines of code.
We can do this by abstracting away the logic into macros.

It is important to balance the readability/maintainability of the code with how concise it is.
Always remember that multiple people might be using this code, so be mindful and intentional about where you use macros.

## Packages

Packages are a tool for importing models and macros into your dbt Project.
These may have been written in by a coworker or someone else in the dbt community that you have never met.

Installation:
- Packages are configured in the root of your dbt project in a file called `packages.yml`.
- You can adjust the version to be compatible with your working version of dbt.
- Packages are then installed with the command `dbt deps`.

## Materialization

Materializations are strategies for persisting dbt models in a warehouse.
There are the following types of materializations built into dbt. They are:

- table
- view
- incremental
- ephemeral
- snapshot

By default, dbt models are materialized as views.

### Tables
- Built as tables in the database
- Data is stored on disk
- Slower to build
- Faster to query

### Views
- Built as views in the database
- Query is stored on disk
- Faster to build
- Slower to query

### Ephemeral
- Does not exist in the database
- Imported as CTE into downstream models
- Increases build time of downstream models
- Cannot query directly

### Incremental 
- Built as table in the database
- On the first run, builds entire table
- On subsequent runs, only appends new records*
- Faster to build because you are only adding new records
- Does not capture 100% of the data all the time

Bonus: [On the limits of incrementality](https://discourse.getdbt.com/t/on-the-limits-of-incrementality/)

### Snapshots
- Built as a table in the database, usually in a dedicated schema.
- On the first run, builds entire table
- In future runs, dbt will scan the underlying data and append new records based on the configuration that is made
- This allows you to capture historical data

## Analyses

Analyses are SQL files that live in the `analysis/` folder.
Analyses will not be run with dbt run like models.
However, you can still compile these from Jinja-SQL to pure SQL using dbt compile.
These will compile to the target folder.
Analyses are useful for training queries, one-off queries, and audits.

## Seeds
Seeds are CSV files that live in the data folder.
When executing `dbt run`, seeds will be built in your Data Warehouse as tables.
Seeds can be references using the ref macro - just like models!

Seeds should be used for data that doesn't change frequently.
Seeds should not be the process for uploading data that changes frequently.
Seed are useful for loading country codes, employee emails, or employee account IDs.
