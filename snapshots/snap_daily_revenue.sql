{% snapshot orders_snapshot %}



{{
    config(
        target_database='hellodbt',
        target_schema='dbt_snapshots',
        unique_key='order_date',

        strategy='check',
        check_cols=['daily_revenue']
    )
}}

select
    order_date,
    sum(amount) as daily_revenue
from {{ ref('fct_orders') }}
group by 1
order by 1

{% endsnapshot %}
